part of game_entities;

class Coin extends Entity {

  AnimatedSprite _coinSprite;
  Sprite _coinShadowSprite;

  Coin(Vec3 position, World world, Assets assets) : super(position, world) {
    hitbox = new Hitbox(this, 0, -8, 16, 16);
    _coinSprite = new AnimatedSprite(assets.spriteCoin, 0.1)
      ..currentFrame = new Random().nextInt(8);
    _coinShadowSprite = new StaticSprite(assets.spriteCoinShadow, 0, 0);

    maxGroundSpeed = 100.0;
    maxAirSpeed = 1000.0;
    groundFriction = 500.0;
  }

  void jump(Random rng, double maxSpeed) {
    speed.x = maxSpeed * rng.nextDouble() * (rng.nextBool() ? -1 : 1);
    speed.y = maxSpeed * rng.nextDouble() * (rng.nextBool() ? -1 : 1);
    speed.z = 200.0 + rng.nextDouble()*50;
  }

  @override
  void update(double delta) {
    Player player = world.player;

    acceleration.lengthXY = 0.0;
    if (position.z <= 0 && this.position.distance(player.position) < 50) {
      Vec3 acc = new Vec3.directional(this.position, player.position);
      acc.length = 1500.0;
      acceleration += acc;
    }

    super.update(delta);
  }

  onCollision(Entity e) {
    if (position.z <= 0 && e is Player) {
      world.removeEntity(this);
    }
  }

  @override
  void renderPrimary(CanvasRenderingContext2D context, double delta) {
    _coinSprite.drawCentered(context, position.x.round(),
    position.y.round() - _coinSprite.height~/2 - position.z.round(), delta);
  }

  @override
  void renderSecondary(CanvasRenderingContext2D context, double delta) {
    context.globalAlpha = 0.2 - min(50, position.z)/50*0.2;
    _coinShadowSprite.drawCentered(context, position.x.round(), position.y.round(), delta);
    context.globalAlpha = 1;
  }
}