library game_sprites;

import 'dart:html';

class Area {

  int _x, _y, _width, _height;
  Area(this._x, this._y, this._width, this._height);

  get x => _x;
  get y => _y;
  get width => _width;
  get height => _height;
}

class SpriteSheet {

  final ImageElement image;
  final int spriteWidth, spriteHeight;
  int _verticalSize, _horizontalSize;

  List<List<Area>> areas = [];

  SpriteSheet(ImageElement this.image, int this.spriteWidth, int this.spriteHeight) {
    _verticalSize = image.height ~/ spriteHeight;
    _horizontalSize = image.width ~/ spriteWidth;

    for (int i = 0; i < _verticalSize; i++) {
      List<Area> tempAreas = [];
      areas.add(tempAreas);
      for (int o = 0; o < _horizontalSize; o++) {
        tempAreas.add(new Area(spriteWidth * o, spriteHeight * i, spriteWidth, spriteHeight));
      }
    }
  }

  Area getArea(int horizontal, int vertical) {
    return areas[vertical][horizontal];
  }

  int get verticalSize => _verticalSize;
  int get horizontalSize => _horizontalSize;
}

abstract class Sprite {
  int verticalIndex = 0;
  int get width;
  int get height;

  draw(CanvasRenderingContext2D context, int x, int y, double delta);

  drawCentered(CanvasRenderingContext2D context, int x, int y, double delta) {
    draw(context, x - width~/2, y - height~/2, delta);
  }
}

class AnimatedSprite extends Sprite {

  SpriteSheet spriteSheet;
  double frameLength;
  double _timePassed = 0.0;
  int currentFrame;

  int min, max;

  AnimatedSprite(SpriteSheet this.spriteSheet, double this.frameLength) {
    min = 0;
    max = spriteSheet._horizontalSize - 1;
  }

  @override
  draw(CanvasRenderingContext2D context, int x, int y, double delta) {
    if (currentFrame == null)
      currentFrame = min;

    _timePassed += delta;
    while (_timePassed > frameLength) {
      currentFrame++;
      if (currentFrame > max)
        currentFrame = min;
      _timePassed -= frameLength;
    }
    Area area = spriteSheet.getArea(currentFrame, verticalIndex);
    context.drawImageScaledFromSource(spriteSheet.image, area.x, area.y, area.width, area.height, x, y, area.width, area.height);
  }

  @override
  int get height => spriteSheet.spriteHeight;

  @override
  int get width => spriteSheet.spriteWidth;
}

class StaticSprite extends Sprite {

  SpriteSheet spriteSheet;
  int horizontal, vertical;

  StaticSprite(SpriteSheet this.spriteSheet, int this.horizontal, int this.vertical);

  @override
  draw(CanvasRenderingContext2D context, int x, int y, double delta) {
    Area area = spriteSheet.getArea(horizontal, vertical);
    context.drawImageScaledFromSource(spriteSheet.image, area.x, area.y, area.width, area.height, x, y, area.width, area.height);
  }

  @override
  int get height => spriteSheet.spriteHeight;

  @override
  int get width => spriteSheet.spriteWidth;
}