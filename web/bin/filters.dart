import 'dart:html';

class Filter {
  ImageData _pixels;
  ImageData get pixels => _pixels;

  int width, height;

  Filter(Element img) {
    if (img is CanvasElement) {
      _pixels = img.context2D.getImageData(0, 0, img.width, img.height);
      width = img.width;
      height = img.height;
    }
    else if (img is ImageElement) {
      var canvas = new CanvasElement(width: img.width, height: img.height);
      CanvasRenderingContext2D context = canvas.getContext('2d');
      context.drawImage(img, 0, 0);
      _pixels = context.getImageData(0, 0, canvas.width, canvas.height);
      width = img.width;
      height = img.height;
    }
  }

  ImageData createImageData(int width, int height) {
    CanvasElement temp = new CanvasElement(width: width, height: height);
    return temp.context2D.createImageData(width, height);
  }

  void grayscale(double interpolation) {
    interpolation = 1 - interpolation;
    var d = _pixels.data;
    for (var i = 0; i < d.length; i += 4) {
      var r = d[i];
      var g = d[i + 1];
      var b = d[i + 2];
      var v = (0.2126 * r).toInt() + (0.7152 * g).toInt() + (0.0722 * b).toInt();
      d[i]     = _mid(v, r, interpolation);
      d[i + 1] = _mid(v, g, interpolation);
      d[i + 2] = _mid(v, b, interpolation);
    }
  }

  void lighten(double strength) {
    int add = (255 * strength).toInt();
    var d = _pixels.data;
    for (var i = 0; i < d.length; i += 4) {
      d[i]   = _limited(d[i]   + add);
      d[i+1] = _limited(d[i+1] + add);
      d[i+2] = _limited(d[i+2] + add);
    }
  }

  void darken(double strength) {
    int sub = (255 * strength).toInt();
    var d = _pixels.data;
    for (var i = 0; i < d.length; i += 4) {
      d[i]   = _limited(d[i]   - sub);
      d[i+1] = _limited(d[i+1] - sub);
      d[i+2] = _limited(d[i+2] - sub);
    }
  }

  int _mid(int num1, int num2, double mid) {
    if (num2 > num1) {
      mid = 1-mid;
      int temp = num1;
      num1 = num2;
      num2 = temp;
    }
    return (num1 + (num2 - num1) * mid).toInt();
  }

  int _limited(int value) {
    if (value < 0) return 0;
    if (value > 255) return 255;
    return value;
  }
}