library game_scene;

import 'dart:html';
import 'game.dart';
import 'utils.dart';
import 'world/world.dart';

abstract class Scene {

  CanvasElement canvas;

  Scene(CanvasElement this.canvas);

  void clear() {
    canvas.context2D
      ..fillStyle = "white"
      ..fillRect(0, 0, canvas.width, canvas.height);
  }

  void renderFPS(double delta) {
    canvas.context2D
      ..fillStyle = "black"
      ..fillText("FPS: ${(1/delta).round()}", 3, canvas.height - 3);
  }

  void render(double delta);
}

class GameScene extends Scene {

  World world;

  GameScene(CanvasElement canvas) : super(canvas) {
    world = new World(canvas.width, canvas.height);
  }

  @override
  void render(double delta) {
    clear();

    // If the FPS drops so will the TPS. This code makes sure that the game always runs in at least 30 TPS.
    double rawDelta = delta;
    while (rawDelta > 0) {
      double currentDelta = rawDelta > 1/30 ? 1/30 : rawDelta;
      rawDelta -= currentDelta;
      world.update(currentDelta);
    }

    world.render(canvas.context2D, delta);
    renderFPS(delta);
    canvas.context2D
      ..fillStyle = "black"
      ..fillText("Entities: ${world.entities.length}", 3, canvas.height - 15);
  }
}

class LoadingScene extends Scene {

  ProgressListener listener;
  String message = "Loading assets...";

  int x, y, width, height;

  LoadingScene(CanvasElement canvas, Assets assets) : super(canvas) {
    x = 8;
    y = canvas.height - 18;
    width = 300;
    height = 10;

    listener = new ProgressListener((){
      Game.scene = new GameScene(canvas);
    }, (Error error) {
      message = error.toString();
    });

    assets.load(listener);
  }

  @override
  void render(double delta) {
    clear();
    canvas.context2D
      ..globalAlpha = 1
      ..textAlign = "left"
      ..fillStyle = listener.failed ? "red" : "black"
      ..strokeStyle = "black"
      ..fillText(message, x - 3, y - 7)
      ..strokeRect(x - 2, y - 2, width + 4, height + 4)
      ..fillRect(x, y, width*listener.progress, height);
  }
}