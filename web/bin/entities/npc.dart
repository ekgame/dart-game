part of game_entities;

class NPC extends Entity implements Pathable {

  AnimatedSprite _spriteWalk;
  Sprite _spriteIdle;
  Sprite _spriteShadow;
  final double acc = 1200.0;

  bool isInteracting = false;

  NPC(Vec3 position, World world, Assets assets) : super(position, world) {
    hitbox = new Hitbox(this, 0, 0, 30, 15);
    maxGroundSpeed = 150.0;
    groundFriction = 1000.0;
    //solid = true;
    _spriteWalk = new AnimatedSprite(assets.spriteWalkHuman, 0.1) ..min = 1 ..verticalIndex=2;
    _spriteIdle = new AnimatedSprite(assets.spriteWalkHuman, 0.1) ..max = 0 ..verticalIndex=2;
    _spriteShadow = new StaticSprite(assets.spriteShadow, 0, 0);
  }

  void update(double delta) {
    Player player = world.player;

    //acceleration = new Vec3.directional(this.position, player.position);
    //acceleration.length = acc;

    if (this.position.distance(player.position) < 50) {
      acceleration.length = 0.0;
    }

    super.update(delta);
    confine();
  }

  onCollision(Entity e) {
    /*if (e is NPC) {
      world.removeEntity(this);
      world.removeEntity(e);
    }*/
  }

  Sprite getSprite() {
    if (speed.lengthXY > 0) {
      _spriteWalk.frameLength = 0.06 + 0.1*(maxGroundSpeed - speed.length)/maxGroundSpeed;
      int index = 2;
      if (speed.x > 0) index = 3;
      if (speed.x < 0) index = 1;
      if (speed.y > 0 && speed.y.abs() > speed.x.abs()) index = 2;
      if (speed.y < 0 && speed.y.abs() > speed.x.abs()) index = 0;
      _spriteWalk.verticalIndex = index;
      _spriteIdle.verticalIndex = index;
      return _spriteWalk;
    }
    else {
      return _spriteIdle;
    }
  }

  @override
  void renderPrimary(CanvasRenderingContext2D context, double delta) {
    Sprite sprite = getSprite();
    sprite.draw(context, position.x.round() - sprite.width~/2, position.y.round() - sprite.height + 4, delta);
  }

  @override
  void renderSecondary(CanvasRenderingContext2D context, double delta) {
    context.globalAlpha = 0.5;
    _spriteShadow.draw(context, position.x.round() - _spriteShadow.width~/2, position.y.round() - _spriteShadow.height~/2, delta);
    context.globalAlpha = 1;
  }
}