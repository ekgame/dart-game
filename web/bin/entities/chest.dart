part of game_entities;

class Chest extends Entity implements Interactable {

  static final StaticSprite spriteOpen   = new StaticSprite(Game.assets.spriteChest, 1, 0);
  static final StaticSprite spriteClosed = new StaticSprite(Game.assets.spriteChest, 0, 0);
  static final StaticSprite spriteShadow = new StaticSprite(Game.assets.spriteChestShadow, 0, 0);
  int height = 20;

  bool open = false;

  Chest(Vec3 position, World world) : super(position, world) {
    hitbox = new Hitbox(this, 0, -height~/2, spriteClosed.width - 4, height);
    solid = true;
  }

  @override
  void renderPrimary(CanvasRenderingContext2D context, double delta) {
    Sprite sprite = open ? spriteOpen : spriteClosed;
    sprite.draw(context, position.x.round() - sprite.width~/2, position.y.round() - sprite.height, delta);
  }

  @override
  void renderSecondary(CanvasRenderingContext2D context, double delta) {
    spriteShadow.draw(context, position.x.round() - spriteShadow.width~/2, position.y.round() - spriteShadow.height + 10, delta);
  }

  @override
  void interact(Entity entity) {
    open = !open;

    if (open) {
      Random random = new Random();
      Vec3 pos = position.clone + new Vec3(0.0, -2.0, 1.0);
      for (int i = 0; i < 20; i++) {
        Coin coin = new Coin(pos.clone, world, Game.assets);
        coin.jump(random, 50.0);
        world.addEntity(coin);
      }
    }
  }
}