import '../entities/entities.dart';
import '../math.dart';
import '../game.dart';
import 'path.dart';
import 'dart:collection';
import 'dart:html';

class World {

  int width, height;

  List<Entity> entities = [];
  List<Entity> toRemove = [];
  List<Entity> toAdd = [];
  Player player;
  NPC npc;

  Paths paths;

  World(int this.width, int this.height) {
    paths = new Paths(100, this);
    addEntity(player = new Player(new Vec3.XY(80.0, 80.0), this, Game.assets));
    addEntity(new Chest(new Vec3.XY(200.0, 200.0), this));
    addEntity(npc = new NPC(new Vec3.XY(400.0, 400.0), this, Game.assets));
  }

  void update(double delta) {
    for (Entity entity in toRemove)
      entities.remove(entity);
    toRemove.clear();

    for (Entity entity in toAdd)
      entities.add(entity);
    toAdd.clear();

    for (Entity entity in entities) {
      entity.update(delta);
      entity.handleEntityCollisions(entities);
    }
  }

  void render(CanvasRenderingContext2D context, double delta) {
    SplayTreeMap<double, Entity> queue = new SplayTreeMap();
    for (Entity entity in entities) {
      double pos = entity.position.y;
      while (queue.containsKey(pos))
        pos -= 0.0001; // There may be entities in the same Y position, so the key is offset
      queue[pos] = entity;
    }

    for (Entity entity in queue.values)
      entity.renderSecondary(context, delta);

    for (Entity entity in queue.values)
      entity.renderPrimary(context, delta);

    // debug paths
    paths.update();
    /*for (PathPoint point in paths.points) {
      for (PathPoint other in point.connections) {
        context
          ..strokeStyle = "black"
          ..lineWidth = 1
          ..beginPath()
          ..moveTo(point.x, point.y)
          ..lineTo(other.x, other.y)
          ..closePath()
          ..stroke();
      }
    }

    for (PathPoint point in paths.points) {
      context
        ..strokeStyle = point is PathPointDynamic ? "red" : "black"
        ..beginPath()
        ..arc(point.x, point.y, 5, 0, 7)
        ..closePath()
        ..stroke();
    }*/

    List<PathPoint> points = paths.getPathEntity(npc, player);
    int i = 0;
    print("pnt ${points.length}");
    for (PathPoint point in points) {

      if (i == 0) {

        context
          ..strokeStyle = "green"
          ..lineWidth = 2
          ..beginPath()
          ..moveTo(point.x, point.y);
      }
      else {

        context
          ..lineTo(point.x, point.y);
      }
      i++;
    }
    context
      ..closePath()
      ..stroke();
  }

  void addEntity(Entity e) {
    toAdd.add(e);
    if (e is Pathable)
      paths.addDynamicPoint(e);
  }

  void removeEntity(Entity e) {
    toRemove.add(e);
    if (e is Pathable)
      paths.removeDynamicPoint(e);
  }
}