part of game_entities;

class Player extends Entity implements Pathable {

  AnimatedSprite _spriteWalk;
  Sprite _spriteIdle;
  Sprite _spriteShadow;
  final double acc = 2000.0;

  bool isInteracting = false;

  Player(Vec3 position, World world, Assets assets) : super(position, world) {
    hitbox = new Hitbox(this, 0, 0, 30, 15);

    maxGroundSpeed = 200.0;
    groundFriction = 1000.0;
    _spriteWalk = new AnimatedSprite(assets.spriteWalk, 0.1) ..min = 1 ..verticalIndex=2;
    _spriteIdle = new AnimatedSprite(assets.spriteWalk, 0.1) ..max = 0 ..verticalIndex=2;
    _spriteShadow = new StaticSprite(assets.spriteShadow, 0, 0);
  }

  void update(double delta) {
    if (Game.isKeyDown(Key.UP))         acceleration.y = -acc;
    else if (Game.isKeyDown(Key.DOWN))  acceleration.y = acc;
    else acceleration.y = 0.0;

    if (Game.isKeyDown(Key.LEFT))       {acceleration.x = -acc; acceleration.length = acc;}
    else if (Game.isKeyDown(Key.RIGHT)) {acceleration.x = acc; acceleration.length = acc;}
    else acceleration.x = 0.0;

    super.update(delta);
    confine();

    if (Game.isKeyDown(Key.INTERACT) && !isInteracting) {
      isInteracting = true;
      interact();
    }

    if (!Game.isKeyDown(Key.INTERACT))
      isInteracting = false;
  }

  void interact() {
    Entity closest = null;
    world.entities.where((e) => e is Entity && e is Interactable).forEach((Entity e) {
      if (closest == null)
        closest = e;
      else if (closest.position.distance(this.position) > e.position.distance(this.position))
        closest = e;
    });

    double interactionDistance = 20.0;

    if (closest != null && closest.position.distance(this.position) <= interactionDistance) {
      (closest as Interactable).interact(this);
    }
  }

  Sprite getSprite() {
    if (speed.lengthXY > 0) {
      _spriteWalk.frameLength = 0.06 + 0.1*(maxGroundSpeed - speed.length)/maxGroundSpeed;
      int index = 2;
      if (speed.x > 0) index = 3;
      if (speed.x < 0) index = 1;
      if (speed.y > 0 && speed.y.abs() > speed.x.abs()) index = 2;
      if (speed.y < 0 && speed.y.abs() > speed.x.abs()) index = 0;
      _spriteWalk.verticalIndex = index;
      _spriteIdle.verticalIndex = index;
      return _spriteWalk;
    }
    else {
      return _spriteIdle;
    }
  }

  @override
  void renderPrimary(CanvasRenderingContext2D context, double delta) {
    Sprite sprite = getSprite();
    sprite.draw(context, position.x.round() - sprite.width~/2, position.y.round() - sprite.height + 4, delta);
  }

  @override
  void renderSecondary(CanvasRenderingContext2D context, double delta) {
    context.globalAlpha = 0.5;
    _spriteShadow.draw(context, position.x.round() - _spriteShadow.width~/2, position.y.round() - _spriteShadow.height~/2, delta);
    context.globalAlpha = 1;
  }
}