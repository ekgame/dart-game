library game_math;

import 'dart:math';
import 'entities/entities.dart';
import 'dart:html';

class Vec3 {
  double x = 0.0, y = 0.0, z = 0.0;

  Vec3(this.x, this.y, this.z);

  Vec3.XY(this.x, this.y) {
    this.z = 0.0;
  }

  Vec3.directional(Vec3 from, Vec3 to) {
    x = to.x - from.x;
    y = to.y - from.y;
    z = to.z - from.z;
  }

  Vec3.zero();

  operator *(double scale) {
    x *= scale;
    y *= scale;
    z *= scale;
    return this;
  }

  operator /(double scale) {
    x /= scale;
    y /= scale;
    z /= scale;
    return this;
  }

  operator +(Vec3 other) {
    x += other.x;
    y += other.y;
    z += other.z;
    return this;
  }

  operator -(Vec3 other) {
    x -= other.x;
    y -= other.y;
    z -= other.z;
    return this;
  }

  double get length => sqrt(x*x + y*y + z*z);
  double get lengthXY => sqrt(x*x + y*y);

  set length(double newLength) {
    double len = length;
    if (len <= 0)
      len = 1.0;

    x = x/len*newLength;
    y = y/len*newLength;
    z = z/len*newLength;
  }

  set lengthXY(double newLength) {
    double len = length;
    if (len <= 0)
      len = 1.0;

    x = x/len*newLength;
    y = y/len*newLength;
  }

  rotateXY(double radians) {
    x = x*cos(radians) - y*sin(radians);
    y = x*sin(radians) + y*cos(radians);
  }

  double distance(Vec3 other) => sqrt(_sqr(x-other.x) + _sqr(y-other.y) + _sqr(z-other.z));
  double _sqr(x) => x*x;

  Vec3 get clone => new Vec3(x, y, z);
}

class Hitbox {
  Entity entity;
  int x, y, width, height;

  Hitbox(this.entity, this.x, this.y, this.width, this.height);

  render(CanvasRenderingContext2D context, String color) {
    context
      ..strokeStyle = color
      ..fillStyle = color
      ..globalAlpha = 0.3
      ..fillRect(entity.position.x + x - width/2, entity.position.y + y - height/2, width, height)
      ..globalAlpha = 1
      ..strokeRect(entity.position.x + x - width/2, entity.position.y + y - height/2, width, height);
  }

  double get left => entity.position.x + x - width/2;
  double get right => entity.position.x + x + width/2;
  double get top => entity.position.y + y - height/2;
  double get bottom => entity.position.y + y + height/2;

  set left(double xPos) => entity.position.x = xPos - x + width/2;
  set right(double xPos) => entity.position.x = xPos - x - width/2;
  set top(double yPos) => entity.position.y = yPos - y + height/2;
  set bottom(double yPos) => entity.position.y = yPos - y - height/2;

  Vec3 collision(Hitbox other) {
    double intLeft = other.right - left;
    double intRight = right - other.left;
    double intTop = other.bottom - top;
    double intBottom = bottom - other.top;

    if (intLeft < 0 || intRight < 0) return null;
    if (intTop < 0 || intBottom < 0) return null;

    Vec3 result = new Vec3.zero();

    result.x = intLeft < intRight ? -intLeft : intRight;
    result.y = intTop < intBottom ? -intTop : intBottom;

    if (result.x.abs() < result.y.abs())
      result.y = 0.0;
    else
      result.x = 0.0;

    return result;
  }
}