library game_core;

import 'dart:html';
import 'sprites.dart';
import 'utils.dart';
import 'scene.dart';

class Game {

  static final Game _instance = new Game._internal();
  factory Game() => _instance;

  Assets _assets = new Assets("assets");
  static Assets get assets => _instance._assets;

  List<int> pressedKeys = [];
  Scene _currentScene;

  static Scene get scene => _instance._currentScene;
  static set scene(Scene scene) => _instance._currentScene = scene;

  Game._internal() {
    window.addEventListener("keydown", (event) {
      if (!pressedKeys.contains(event.keyCode))
        pressedKeys.add(event.keyCode);
    });
    window.addEventListener("keyup", (event) {
      pressedKeys.remove(event.keyCode);
    });
  }

  static bool isKeyDown(int key) => _instance.pressedKeys.contains(key);

  loadAssets(ProgressListener listener){
    _assets.load(listener);
  }

  static void start(CanvasElement canvas) {
    LoadingScene loading = new LoadingScene(canvas, assets);
    scene = loading;
  }
}

class Assets {

  SpriteSheet spriteWalk;
  SpriteSheet spriteShadow;
  SpriteSheet spriteChest;
  SpriteSheet spriteChestShadow;
  SpriteSheet spriteWalkHuman;
  SpriteSheet spriteCoin;
  SpriteSheet spriteCoinShadow;

  String _folder;
  List<String> _imageAssets = [];

  Assets(String this._folder) {
    _image("BODY_skeleton");
    _image("chest");
    _image("shadow");
    _image("chest_shadow");
    _image("BODY_male");
    _image("spinning_coin_gold");
    _image("shadow_coin");
  }

  _image(String filename) => _imageAssets.add("$_folder/$filename.png");

  void load(ProgressListener listener) {
    int loaded = 0;
    List<ImageElement> images = [];
    for (String filename in _imageAssets) {
      ImageElement element = new ImageElement(src: filename);
      images.add(element);
      element.onLoad.listen((event) {
        loaded++;
        listener.progress = (loaded/_imageAssets.length)*0.99;
        if (loaded >= _imageAssets.length)
          _onImagesLoaded(images, listener);
      });
      element.onError.listen((event) {
        listener.fail(new LoadingError(filename));
      });
    }
  }

  void _onImagesLoaded(List<ImageElement> images, ProgressListener listener) {
    spriteWalk   = new SpriteSheet(images[0], 64, 64);
    spriteChest  = new SpriteSheet(images[1], 32, 32);
    spriteShadow = new SpriteSheet(images[2], 32, 16);
    spriteChestShadow = new SpriteSheet(images[3], 48, 48);
    spriteWalkHuman   = new SpriteSheet(images[4], 64, 64);
    spriteCoin = new SpriteSheet(images[5], 16, 16);
    spriteCoinShadow = new SpriteSheet(images[6], 16, 8);
    listener.progress = 1.0;
  }
}