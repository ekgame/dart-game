import 'world.dart';
import '../entities/entities.dart';
import 'dart:math';

class Paths {

  List<PathPoint> points = [];
  Map<Entity, PathPointDynamic> dynamicPoints = {};
  int density;

  Paths(int this.density, World world) {
    for (double x = density/2; x < world.width; x += density)
      for (double y = density/2; y < world.height; y += density)
        points.add(new PathPoint(x, y));

    makeConnections();
  }

  void makeConnections() {
    for (PathPoint point1 in points) {
      point1.connections.clear();
      for (PathPoint point2 in points) {
        if (point1 == point2)
          continue;
        if (point1.distance(point2) < density * 2.3)
          point1.connections.add(point2);
      }
    }
  }

  void addDynamicPoint(Entity entity) {
    PathPoint point = new PathPointDynamic(entity);
    dynamicPoints[entity] = point;
    points.add(point);
  }

  void removeDynamicPoint(Entity entity) {
    PathPoint value = dynamicPoints.remove(entity);
    points.remove(value);
  }

  List<PathPoint> getPath(PathPoint from, PathPoint to) {
    for (PathPoint point in points)
      point.reset();

    List<PathPoint> openPoints = [from];
    List<PathPoint> closedPoints = [];
    bool found = false;

    loop:
    while (!openPoints.isEmpty) {
      List<PathPoint> toRemove = [];
      List<PathPoint> toAdd = [];
      for (PathPoint point in openPoints) {
        if (closedPoints.contains(point) || toRemove.contains(point))
          continue;
        toRemove.add(point);
        closedPoints.add(point);
        for (PathPoint child in point.connections) {
          child.parent = point;
          child.dist = point.dist + point.distance(child);
          if (child == to) {
            found = true;
            break loop;
          }
          if (!toAdd.contains(child))
            toAdd.add(child);
        }
      }
      for (PathPoint point in toRemove)
        openPoints.remove(point);
      for (PathPoint point in toAdd)
        openPoints.add(point);
    }

    if (found) {
      print("found");
      List<PathPoint> points = [];
      PathPoint root = to;
      while (root != null) {

        points.add(root);
        root = root.parent;
      }
      return points;
    }
    return null;
  }

  List<PathPoint> getPathEntity(Entity from, Entity to) {
    if (from is Pathable && to is Pathable)
      return getPath(dynamicPoints[from], dynamicPoints[to]);
    return null;
  }

  void update() {
    // Remove connections to dynamic points
    dynamicPoints.values.forEach((current) {
      for (PathPoint point in current.connections)
        point.connections.remove(current);
      current.connections.clear();
    });

    dynamicPoints.values.forEach((current) {
      for (PathPoint point in points) {
        if (point == current)
          continue;
        if (current.distance(point) < density * 2.3) {
          current.connections.add(point);
          point.connections.add(current);
        }
      }
    });
  }
}

class PathPoint {

  double x = 0.0, y = 0.0;
  List<PathPoint> connections = [];

  PathPoint parent = null;
  double dist = 0.0;

  PathPoint.zero();
  PathPoint(double this.x, double this.y);

  double distance(PathPoint other) {
    double xx = x - other.x;
    double yy = y - other.y;
    return sqrt(xx*xx + yy*yy);
  }

  void reset() {
    parent = null;
    dist = 0.0;
  }
}

class PathPointDynamic extends PathPoint {
  Entity entity;

  PathPointDynamic(Entity this.entity) : super.zero();

  double get x => entity.position.x;
  double get y => entity.position.y;
}