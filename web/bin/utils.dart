library game_utils;
import 'dart:html';
import 'dart:convert';
import 'dart:math';

class ProgressListener {

  Function onFinished;
  Function onFailed;
  Function onProgressChanged;

  ProgressListener(
      this.onFinished(),
     [this.onFailed(Error error),
      this.onProgressChanged(double previous, double current)]
  );

  bool _failed = false;
  get failed => _failed;

  bool _finished = false;
  get finished => _finished;

  double _progress = 0.0;
  get progress => _progress;

  set progress(double newProgress) {
    if (_failed || finished) return;

    if (newProgress > 1.0)
      newProgress = 1.0;

    double prev = progress;
    _progress = newProgress;

    if (_progress >= 1.0) {
      _finished = true;
      onFinished();
    }

    if (onProgressChanged != null)
      onProgressChanged(prev, _progress);
  }

  fail(Error error) {
    _failed = true;
    if (onFailed != null)
      onFailed(error);
  }
}

class LoadingError extends Error {
  String _filename;
  get filename => _filename;
  LoadingError(this._filename);
  toString() => "Failed to load file \"$_filename\".";
}

class Key {
  static const int
    UP = 38,
    DOWN = 40,
    LEFT = 37,
    RIGHT = 39,
    INTERACT = 32;
}

class TextInput {

  List<int> _ignore = [40, 39, 38, 37];

  Map<int, int> _to_ascii = {
    111: 47,
    188: 44,
    109: 45,
    190: 46,
    191: 47,
    192: 96,
    220: 92,
    222: 39,
    221: 93,
    219: 91,
    173: 45,
    187: 61, //IE Key codes
    186: 59, //IE Key codes
    189: 45, //IE Key codes
  };

  Map<int, String> _shiftUps = {
    96: "~",
    49: "!",
    50: "@",
    51: "#",
    52: "\$",
    53: "%",
    54: "^",
    55: "&",
    56: "*",
    57: "(",
    48: ")",
    45: "_",
    61: "+",
    91: "{",
    93: "}",
    92: "|",
    59: ":",
    39: "\"",
    44: "<",
    46: ">",
    47: "?"
  };

  bool _isCapturing = false;
  bool _shiftDown = false;

  String value = "";

  EventListener listenerDown;
  //EventListener listenerUp;

  TextInput() {
    listenerDown = (event) {
      int code = event.keyCode;
      print("in $code ${event.shiftKey} ${code.runtimeType.toString()}");
      if (code == 8) {
        value = value.substring(0, max(value.length - 1, 0));
      } else if (code == 13) {
        // on enter
      }
      else if (code == 192) {
        // on tilda
      }
      else {
        try {
          if (_ignore.contains(code))
            return;
          if (_to_ascii.containsKey(code))
            code = _to_ascii[code];

          String char;
          if (code >= 96 && code <= 105) {
            char = new String.fromCharCode(code - 48);
          } else if (!event.shiftKey && (code >= 65 && code <= 90)) {
            char = new String.fromCharCode(code + 32);
          } else if (event.shiftKey && _shiftUps.containsKey(code)) {
            char = _shiftUps[code];
          } else {
            char = new String.fromCharCode(code);
          }

          value += char;
        } on Error {/* ignore errors */}
      }
    };
  }

  void capture(bool state) {
    _isCapturing = state;
    if (_isCapturing) {
      _shiftDown = false;
      window.addEventListener("keydown", listenerDown);
      //window.addEventListener("keyup", listenerUp);
      print("capturing");
    }
    else {
      window.removeEventListener("keydown", listenerDown);
      //window.removeEventListener("keyup", listenerUp);
    }
  }

  bool get isCapturing => _isCapturing;
}