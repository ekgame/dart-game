// Copyright (c) 2015, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'dart:html';
import 'bin/game.dart';
import 'bin/scene.dart';
import 'bin/entities/entities.dart';
import 'dart:math';
import 'bin/math.dart';

CanvasElement canvas;

void main() {
  canvas = querySelector('#game-canvas');

  querySelector('#btn-fullscreen').onClick.listen((e){
    canvas.requestFullscreen();
  });

  querySelector('#btn-spawn').onClick.listen((e){
    if (Game.scene is GameScene) {
      Random rng = new Random();
      for (int i = 0 ; i < 100; i++)
      (Game.scene as GameScene).world.entities.add(new Coin(new Vec3.XY(rng.nextDouble()*canvas.width, rng.nextDouble()*canvas.height), (Game.scene as GameScene).world, Game.assets));
    }
  });

  Game.start(canvas);
  window.requestAnimationFrame(draw);
}

num prevTime = 0;

void draw(num time)
{
  double delta = (time - prevTime)/1000;
  prevTime = time;
  Game.scene.render(delta);
  window.requestAnimationFrame(draw);
}