library game_entities;

import '../math.dart';
import 'dart:html';
import 'dart:math';
import '../sprites.dart';
import '../game.dart';
import '../utils.dart';
import '../world/world.dart';
import '../world/path.dart';

part 'chest.dart';
part 'coin.dart';
part 'npc.dart';
part 'player.dart';

abstract class Interactable {
  void interact(Entity entity);
}

abstract class Pathable {
}

abstract class Entity {

  Hitbox hitbox;
  World world;
  Vec3 position;
  Vec3 speed = new Vec3.zero();
  Vec3 acceleration = new Vec3.zero();

  double maxGroundSpeed = 50.0;
  double maxAirSpeed = 500.0;
  double groundFriction = 5.0;
  double airFriction = 1.0;
  double gravity = 500.0;


  bool solid = false;
  bool col = false;

  Entity(Vec3 this.position, World this.world) {
    acceleration.z = -gravity;
  }

  void update(double delta)
  {
    double friction = airFriction;
    double maxSpeed = maxAirSpeed;
    if (position.z <= 0) {
      friction = groundFriction;
      maxSpeed = maxGroundSpeed;
    }

    double newSpeed = max(speed.length - friction*delta, 0.0);
    if (newSpeed > maxSpeed)
      newSpeed = maxSpeed;
    speed.length = newSpeed;

    speed += acceleration.clone * delta;

    position += speed.clone * delta;
    if (position.z < 0)
        position.z = 0.0;
  }

  void confine() {
    if (hitbox == null) return;
    if (hitbox.left < 0) hitbox.left = 0.0;
    if (hitbox.right > world.width) hitbox.right = world.width.toDouble();
    if (hitbox.top < 0) hitbox.top = 0.0;
    if (hitbox.bottom > world.height) hitbox.bottom = world.height.toDouble();
  }

  void handleEntityCollisions(List<Entity> entities) {
    if (this.hitbox == null) return;
    col = false;
    for (Entity entity in entities){
      if (entity == this || entity.hitbox == null) continue;
      Vec3 collision = hitbox.collision(entity.hitbox);
      if (collision != null) {
        if (!col) col = true;
        if (entity.solid) this.position -= collision;
        onCollision(entity);
      }
    }
  }

  onCollision(Entity entity) {}

  void renderPrimary(CanvasRenderingContext2D context, double delta);
  void renderSecondary(CanvasRenderingContext2D context, double delta);
}